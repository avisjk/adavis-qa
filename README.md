## Anthony Davis
## QA Engineer
## anthony@jamkazam.com



## Links to the exercises ##

###[Exercise 1](https://bitbucket.org/avisjk/adavis-qa/src/042bdfd2cb806297bfc88aca7bd84274b44952e5/part-1-spec-review/questions-for-product-manager.md?at=master)


###[Exercise 2 source code](https://bitbucket.org/avisjk/adavis-qa/src/042bdfd2cb806297bfc88aca7bd84274b44952e5/part-2-code-review/code-review.java?at=master)
* [error 1](https://bitbucket.org/avisjk/adavis-qa/commits/71a93ed6705e20499629d8bddf997eb81fe90871?at=master)
* [error 2](https://bitbucket.org/avisjk/adavis-qa/commits/12fad40962f426df07165bf3d164e0a9806f5758?at=master)
* [error 3](https://bitbucket.org/avisjk/adavis-qa/commits/55cc7ff8cb37857b43870cedfa84d2c199f4acf8?at=master)


###[Exercise 3 README](https://bitbucket.org/avisjk/adavis-qa/src/042bdfd2cb806297bfc88aca7bd84274b44952e5/part-3-test-automation/?at=master)
_you'll have to git clone the entire `adavis-qa` project to run Exercise 3_