require 'spec_helper'

RSpec.describe "Create Issues:" do
  let(:user) { TestConfig['user'] || 'test@atlassian.com' }
  let(:pass) { TestConfig['pass'] || 'uncrackable_passwd' }
  let(:dashboard) { SystemDashboardPage.visit }

  specify "when user is not logged in, 'Create' button is not shown" do
    expect(dashboard.create_button_shown?).to be_falsey
  end

  context "when user is logged in" do
    let(:login_page) { LoginPage.visit }
    let(:logged_in_dashboard) { login_page.login_as user, pass }

    specify "clicking 'Create' button allows user to create a bug" do
      summary = "Windows Vista kernel panic"
      description = <<TXT
*Steps to reproduce:*
# Be on Windows Vista
Launch Internet Explorer 6
Dismiss security warnings
Login to JIRA test site

Expected results: Dashboard is shown
Actual results: Computer makes scraping noise and screen is blank
TXT
      create_modal = logged_in_dashboard.click_create_button
      create_modal.select_issue_type 'Bug'
      create_modal.enter_summary summary
      create_modal.enter_description description
      dashboard = create_modal.submit_issue
      expect(dashboard.message_contents).to include "#{summary} has been successfully created."
    end


    specify "pressing 'c' keyboard shortcut will open the create modal" do
      create_modal = logged_in_dashboard.press_create_shortcut
      expect(create_modal).to be_displayed
    end


    context "the standalone non-modal Create Issue page" do
      before { login_page.login_as user, pass }
      let(:create_page) { CreatePage.visit }

      it "shows the issue to user after creation" do
        summary = 'Add voice commands!'
        create_page.select_issue_type 'Improvement'
        create_page.click_next
        create_page.enter_summary summary
        issue_page = create_page.submit_issue

        expect(issue_page.summary).to eq summary
      end
    end
  end
end