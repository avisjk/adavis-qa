
## Anthony Davis
### anthony@jamkazam.com

## Test Automation with Page Object Pattern
### Using Ruby, Capybara and selenium-webdriver

# Set-up (be on Linux or OSX, in a shell)

1. `git clone` this project
2. `cd adavis/part-3-test-automation`
3. `gem install bundler`
4. `bundle install`
5. add your own test site user and password to config.yml !!!
6. `bundle exec rspec`


# Ways I know I could improve this

Ruby isn't statically-typed so it can be hard to tell what type is returned by a method, if anything is returned

Within my Page Objects, currently I have implemented the absolute minimum just to test the Create Issue scenario...
I could do a better job of differentiating between the following Page Object method types:

* element locators (i.e, CSS selectors for each element we care about)
* actions (i.e., clicking, entering text, as well as composite actions such as selecting from a combo-box)
* page content methods (i.e., get the page content within a particular locator)
* boolean methods (i.e., element exists?, element is visible?, etc)

I do not validate that the browser is on the correct page when a new Page Object is instantiated.
_I think this wouldn't be hard but I worry it would introduce race conditions and this is supposed to be a short exercise._