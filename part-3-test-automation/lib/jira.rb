$LOAD_PATH.unshift File.dirname(__FILE__)

require 'jira/test_config'
require 'jira/partials/header'
require 'jira/partials/message'
require 'jira/page'
require 'jira/modal'
Dir[File.dirname(__FILE__) + '/jira/modals/*.rb'].each { |file| require file }
Dir[File.dirname(__FILE__) + '/jira/pages/*.rb'].each { |file| require file }

