module JIRA
  class CreatePage < Page
    # this is not to be confused with the Create modal
    # this is the standalone page (when accessed directly)

    def self.visit
      page.visit "#{root}/secure/CreateIssue!default.jspa"
      CreatePage.new
    end

    def select_issue_type issue_type
      find('#issuetype-field').click
      click_on issue_type
    end

    def click_next
      click_on 'Next'
    end

    def enter_summary text
      fill_in 'summary', with: text
    end

    def enter_description text
      fill_in 'description', with: text
    end

    def submit_issue
      click_on 'issue-create-submit'
      IssuePage.new
    end


    #define methods here to represent additional services the page offers

  end #CreatePage
end #JIRA