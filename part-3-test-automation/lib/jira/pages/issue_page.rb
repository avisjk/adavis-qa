module JIRA
  class IssuePage < Page
    attr_reader :issue

    def self.visit issue
      page.visit "#{root}/browse/#{issue}"
      IssuePage.new
    end

    def initialize
      @issue = page.current_url.split('/').last
    end

    def summary
      find('h1#summary-val').text
    end

    def type
      find('span#type-val').text.strip
    end

    #define methods here to represent additional services the page offers

  end
end