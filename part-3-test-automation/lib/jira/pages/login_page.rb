module JIRA
  class LoginPage < Page

    private

    def perform_login username, password
      within '#form-login' do
        fill_in 'username', with: username
        fill_in 'password', with: password
        click_on 'login-submit'
      end
    end

    public

    def self.visit
      page.visit "#{root}/login.jsp"
      LoginPage.new
    end

    def login_as username, password
      perform_login username, password
      SystemDashboardPage.new
    end

    def login_expecting_failure_as username, password
      perform_login username, password
      LoginPage.new
    end

    #define methods here to represent additional services the page offers

  end
end