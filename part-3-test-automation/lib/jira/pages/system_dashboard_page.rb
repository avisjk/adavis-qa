module JIRA
  class SystemDashboardPage < Page

    def self.visit
      page.visit "#{root}/secure/Dashboard.jspa"
      SystemDashboardPage.new
    end

    #define methods here to represent additional services the page offers

  end
end