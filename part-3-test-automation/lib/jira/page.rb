module JIRA
  class Page
    extend Capybara::DSL
    include Capybara::DSL

    include Header
    include Message

    def self.visit
      raise NotImplementedError.new("visit method not implemented by #{self}")
    end

    def self.root
      TestConfig['site'] || 'https://jira.atlassian.com'
    end

    def press_key key
      page.find('body').native.send_key(key)
    end

    # define methods here for additional services available on *every* page

  end
end