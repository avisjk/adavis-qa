module JIRA
  module Header
    def click_login_button
      within('header') { find('login-link').click }
      LoginPage.new
    end

    def click_create_button
      within('header') { click_on 'create_link' }
      CreateModal.new(self)
    end

    def press_create_shortcut
      press_key 'c'
      CreateModal.new(self)
    end

    def create_button_shown?
      within('header') { has_button? 'create_link' }
    end


    # define additional methods here for services available on *every* header

  end
end