module JIRA
  module Message
    # this is the momentary notification sometimes shown in the top-right corner
    # for example, these are used to show an issue was created successfully

    def message
      find('div.aui-message')
    end
    def message_contents
      message.text
    end

    def message_shown?
      message.visible?
    end

  end
end


