module JIRA
  class Modal < Page
    attr_reader :parent

    def initialize(parent_page)
      @parent = parent_page.class
    end

    def close
      click_on 'Cancel'
      parent.new
    end

    def close_with_escape_key
      press_key :escape
      parent.new
    end
  end
end