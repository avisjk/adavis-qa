module JIRA
  class CreateModal < Modal

    def select_issue_type issue_type
      find('#issuetype-field').click
      click_on issue_type
    end

    def enter_summary text
      fill_in 'summary', with: text
    end

    def enter_description text
      fill_in 'description', with: text
    end

    def submit_issue
      click_on 'create-issue-submit'
      parent.new
    end

    def displayed?
      find('div#create-issue-dialog').visible?
    end

  end
end