require 'yaml'

module JIRA
  class TestConfig
    # this is just a helper to avoid using hard-coded environment/configuration details in test code
    # put stuff in config.yml in the root of this project, or see the README

    def self.config
      @config ||= YAML.load(File.open('config.yml'))
    end

    def self.[]=(key, value)
      config[key] = value
    end

    def self.[](key)
      value = config[key]
      warn "#{self} value not defined for key '#{key}'" if value.nil?
      return value
    end
  end
end