# Part 1 - Spec Review Exercise #
## Anthony Davis ##
### QA Engineer ###



# Questions for Product Manager re: user profile update feature #

1. What is writeable / how does a user become "writeable"? What should be shown to the when a profile is viewed by a non-writeable user?
2. Is a user profile public? If so, what should be shown to non-admin/not-logged-in users viewing the user's profile?
3. Is there a mockup for the admin or Sys Admin viewing a user's profile? What should be different from a user viewing themselves?
4. Should we remove "My Profile" from the admin / Sys Admin view of another user's profile? It seems out of place in the admin mockup
5. The mockup shows "display the groups here" -- is this in scope for the profile update feature? I don't see any details about implementing groups.
6. How should the 'Manage Users' view be rendered? Is there a mockup or rough idea for this?
7. Username, full name: what is the minimum and maximum length of a username? What special characters do we need to support? Any foreign character sets?
8. What is the min/max length of an email address? We can validate that it is a well-formed email at least
9. Will there be a email confirmation step needed once the email is changed?
10. Passwords - what is the minimum and maximum length of a password? Do we need to enforce complexity requirements?
11. Will a password change or profile edit trigger an email to the user informing them?
12. What do we show the user if the password fields do not match? Is there a mockup of this condition?
13. What should we show if text input validations or password complexity requirements are not met? Big red text? A pop-up?
14. The edit profile mockup shows underlined letters for [U]sername, [F]ull Name, and [E]mail.. Are these supposed to function as key commands?
15. Should the modal dialogs close after 'Save' is pressed, or remain open?
16. What do we show when the username or email are already taken? (We're assuming that username and email can't already be in use)


Thanks! Sorry there are so many questions.
Anthony
